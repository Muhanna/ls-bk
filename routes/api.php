<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'Api\AuthController@login');
Route::get('/posts', 'Api\PostController@getAll');

Route::middleware('auth:api')->group(function(){
    Route::post('/posts', 'Api\PostController@create');
    Route::post('/posts/edit', 'Api\PostController@edit');
    Route::delete('/posts/{id}', 'Api\PostController@delete');
    Route::post('/logout', 'Api\AuthController@logout');

});


