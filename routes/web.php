<?php

use Laravel\Passport\Passport;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route url
Route::get('/', function() {
  return view('index');
});
Route::get('/Badr-Aldoweesh/en', 'BadrController@badrEn');
Route::get('/Badr-Aldoweesh/ar', 'BadrController@badrAr');
Route::get('/dashboard', function() {
  return view('pages.dashboard');
})->middleware('check.auth');

Route::get('/admin', function() {
  return view('pages.login');
})->name('login');

Route::post('/login', 'DashboardController@login');
Route::get('/logout', 'DashboardController@logout')->name('logout')->middleware('check.auth');
Route::post('/posts', 'DashboardController@create')->name('posts.create')->middleware('check.auth');
Route::post('/posts/edit', 'DashboardController@edit')->name('posts.edit')->middleware('check.auth');
Route::delete('/posts/{id}', 'DashboardController@delete')->name('posts.delete')->middleware('check.auth');



