
@extends('layouts/contentLayoutMaster')

@section('title', 'Posts')

@section('vendor-style')
        <!-- vendor css files -->
@endsection
@section('page-style')
        <!-- Page css files -->
        <link rel="stylesheet" href="{{ asset(mix('css/pages/dashboard-analytics.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/card-analytics.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/tour/tour.css')) }}">
        <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/core/colors/palette-gradient.css')) }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/file-uploaders/dropzone.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/data-list-view.css')}}">
  @endsection

  @section('content')
    {{-- Dashboard Analytics Start --}}
    <section id="data-thumb-view" class="data-thumb-view-header">
      <div class="action-btns d-none">
      </div>
      <!-- dataTable starts -->
      <div class="table-responsive">
          <table class="table data-thumb-view posts-table">
              <thead>
                  <tr>
                      <th></th>
                      <th>Image</th>
                      <th>Title (English)</th>
                      <th>Content (English)</th>
                      <th>Title (Arabic)</th>
                      <th>Content (Arabic)</th>
                      <th>ACTION</th>
                  </tr>
              </thead>
              <tbody>
              </tbody>
          </table>
      </div>
      <!-- dataTable ends -->

      <!-- add new sidebar starts -->
      <div class="add-new-data-sidebar">
          <div class="overlay-bg"></div>
          <div class="add-new-data">
              <div class="div mt-2 px-2 d-flex new-data-title justify-content-between">
                  <div>
                      <h4 class="text-uppercase">ADD POST</h4>
                  </div>
                  <div class="hide-data-sidebar">
                      <i class="feather icon-x"></i>
                  </div>
              </div>
              <div class="data-items pb-3">
                  <div class="data-fields px-2 mt-3">
                      <div class="row">
                          <div class="col-sm-12 data-field-col">
                              <label for="data-name">Title (English)</label>
                              <input type="text" class="form-control" id="post-title-english">
                          </div>
                          <div class="col-sm-12 data-field-col">
                              <label for="data-category"> Content (English)</label>
                              <textarea id="post-content-english" class="form-control" rows="5"></textarea>
                          </div>
                          <div class="col-sm-12 data-field-col">
                              <label for="data-name">Title (Arabic)</label>
                              <input type="text" class="form-control" id="post-title-arabic">
                          </div>
                          <div class="col-sm-12 data-field-col">
                              <label for="data-category"> Content (Arabic)</label>
                              <textarea id="post-content-arabic" class="form-control" rows="5"></textarea>
                          </div>
                          <div class="col-sm-12 data-field-col data-list-upload">
                              <form action="#" class="dropzone dropzone-area" style="display: none" id="dataListUpload">
                                  <div class="dz-message">Upload Image</div>
                              </form>
                              <input type="file" name="file" id="post-image">
                          </div>
                      </div>
                  </div>
              </div>
              <div class="add-data-footer d-flex justify-content-around px-3 mt-2">
                  <div class="add-data-btn">
                      <button class="btn btn-primary" id="add-post">Confirm</button>
                  </div>
                  <div class="cancel-data-btn">
                      <button class="btn btn-outline-danger" id="cancel-add-post">Cancel</button>
                  </div>
              </div>
          </div>
      </div>
      <!-- add new sidebar ends -->
  </section>
  <!-- Dashboard Analytics end -->
  @endsection

@section('vendor-script')
        <!-- vendor files -->
@endsection
@section('page-script')
        <!-- Page js files -->
        <script src="{{ asset(mix('vendors/js/extensions/dropzone.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.select.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="{{ asset(mix('js/core/app-menu.js')) }}"></script>
        <script src="{{ asset(mix('js/core/app.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/components.js')) }}"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="{{ asset(('app-assets/js/scripts/ui/data-list-view.js')) }}"></script>

        {{-- Custom Js --}}
        <script src="{{ asset('custom/js/posts.js')}}"></script>
@endsection
