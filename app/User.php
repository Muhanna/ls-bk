<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
  use Notifiable, HasApiTokens;

  protected $fillable = [
    'name', 'password',
  ];

  protected $hidden = [
    'password', 'remember_token',
  ];
}

// class User extends Model
// {
//     protected $table = 'users';

//     public $timestamps = false;

//     public function login($username, $password)
//     {
//       $user = $this->where('username', $username)->first();

//       if(!$user)
//         return 0;

//       if($user->password != $password)
//         return 0;

//       return 1;
//     }
// }
