<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Post;

use App\User;

class DashboardController extends Controller
{
    function __construct(User $user)
    {
      $this->user = $user;
    }

    public function login(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'username' => ['required', 'string'],
        'password' => ['required', 'string']
      ]);

      if($validator->fails())
        return response()->json(['success' => 0, 'error' => 'You sent invalid credential']);

      $username = $request->username;
      
      $result = Auth::attempt(['name' => $request->username, 'password' => $request->password]);
      if(!$result)
        return response()->json(['success' => 0, 'error' => 'You sent invalid credential'], 200);

      session(['username' => $username]);
      return response()->json(['success' => 1, 'token' => 3434343434], 200);
    }

    public function logout(Request $request)
    {
      $request->session()->flush();
      return redirect()->route('login');
    }

    public function create(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'title_english' => ['required', 'string', 'max:255'],
        'content_english' => ['required', 'string', 'max:255'],
        'title_arabic' => ['required', 'string', 'max:255'],
        'content_arabic' => ['required', 'string', 'max:255'],
        'image' => ['required','file', 'mimes:jpeg,jpg,png']
      ]);

      if($validator->fails())
        return response()->json([
          'success' => 0,
          'data' => $validator->errors()->all()
        ]);

      $post = new Post;
      $post->title_english = $request->title_english;
      $post->content_english = $request->content_english;
      $post->title_arabic = $request->title_arabic;
      $post->content_arabic = $request->content_arabic;
      $post->image = basename($request->image->store('public/upload/posts'));
      $post->save();

      return response()->json(['success' => 1], 200);
    }

    public function edit(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'id' => ['required'],
        'title_english' => ['required', 'string', 'max:255'],
        'content_english' => ['required', 'string', 'max:255'],
        'title_arabic' => ['required', 'string', 'max:255'],
        'content_arabic' => ['required', 'string', 'max:255'],
        'image' => ['nullable','file', 'mimes:jpeg,jpg,png']
      ]);

      if($validator->fails())
        return response()->json([
          'success' => 0,
          'data' => $validator->errors()->all()
        ]);

      $id = $request->id;
      $title_english = $request->title_english;
      $content_english = $request->content_english;
      $title_arabic = $request->title_arabic;
      $content_arabic = $request->content_arabic;
      $image = !!$request->image ? basename($request->image->store('/public/upload/posts')) : null;
      $post = Post::find($id);
      $post->edit($id, $title_english, $content_english, $title_arabic, $content_arabic, $image);

      return response()->json(['success' => 1], 200);
    }

    public function delete($id)
    {
      $post = Post::find($id);
      $post->delete();

      return response()->json(['success' => 1], 200);
    }
}

