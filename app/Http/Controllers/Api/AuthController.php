<?php

namespace App\Http\Controllers\API;

use App\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Laravel\Passport\Passport;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;



class AuthController extends Controller
{
    use ApiResponser;
    //
    // function __construct(User $user)
    // {
    //   $this->user = $user;
    // }

    public function login(Request $request)
    {
        $attr = $this->validateLogin($request);

        if (!Auth::attempt(['name' => $request->username, 'password' => $request->password])){
            return $this->error('Credentials mismatch', 401);
        }

        return $this->token($this->getPersonalAccessToken());

        // return response()->json($this->getPersonalAccessToken(), 200);

        // if (auth()->attempt(['name' => $request->username, 'password' => md5($request->password)])){
        //     $user = auth()->user();
        //     $accessToken = $user->createToken('authToken')->accessToken;

        //     // return response
        //     $response = [
        //         'success' => true,
        //         'message' => 'User login successful',
        //         'accessToken' => $accessToken,
        //         'user_id' => $user->id
        //     ];
        //     return response()->json($response, 200);

        // } else {
        //     // return response
        //     $response = [
        //         'success' => false,
        //         'message' => 'Unauthorised',
        //     ];
        //     return response()->json($response, 404);
        // }
        // if (Auth::attempt(['name' => $request->username, 'password' => md5($request->password)])){
        //     $response = [
        //         'name' => $request->username,
        //         'password' => md5($request->password)                
        //     ];
        //     return response()->json($response, 200);
        // }
        


        // return response(['user' => auth()->user(), 'access_token' => $accessToken]);

    }

    public function user(){
        return $this->success(Auth::user());
    }

    public function logout(){
        Auth::user()->token()->revoke();
        return $this->success('User Logged Out', 200);
    }

    public function getPersonalAccessToken(){
        return Auth::user()->createToken('Personal Access Token');
    }

    public function validateLogin($request){
        return $request->validate([
            'username' => 'required|string',
            'password' => 'required|string'
        ]);
    }
}
