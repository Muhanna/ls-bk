<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    public function edit($id, $title_english, $content_english, $title_arabic, $content_arabic, $image)
    {
      $post = $this->find($id);
      $post->title_english = $title_english;
      $post->content_english = $content_english;
      $post->title_arabic = $title_arabic;
      $post->content_arabic = $content_arabic;
      if(!!$image)
        $post->image = $image;

      $post->save();

      return;
    }
}
