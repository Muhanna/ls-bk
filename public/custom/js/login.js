
$(document).ready(() => {
  $('button#btn-login').click(() => {
    let username = $('input#inpt-username').val()
    let password = $('input#inpt-password').val()

    let data = new FormData()
    data.append('username', username)
    data.append('password', password)

    $.ajax({
      type: 'POST',
      url: '/login',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      processData: false,
      contentType: false,
      data: data,
      success: res => {
        
        if(!res.success)
          return alert(res.error)

        window.location = '/dashboard';

        return
      },
      error:  err => console.log(err)
    })
  })
})
