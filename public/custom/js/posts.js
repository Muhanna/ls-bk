

let mode = 1
let editId = undefined

$(document).ready(function() {

  getPosts()

  $(document).on('click', 'div.actions button', () => {
    mode = 1
  })

  $(document).on('click', 'span.action-edit', function() {
    $thisTr = $(this).parents('tr')
    $('input#post-title-english').val($thisTr.find('.title-english').text())
    $('textarea#post-content-english').val($thisTr.find('.content-english').text())
    $('input#post-title-arabic').val($thisTr.find('.title-arabic').text())
    $('textarea#post-content-arabic').val($thisTr.find('.content-arabic').text())

    editId = $thisTr.attr('data-id')
    mode = 0
  })

  $(document).on('click', 'span.action-delete', function() {
    $thisTr = $(this).parents('tr')
    deleteId = $thisTr.attr('data-id')
    return deletePost(deleteId)
  })

  $('button#add-post').click(() => {
    if(!!mode)
      return addPost()

    return editPost()
  })

})

const getPosts = () => {
  $.ajax({
    type: 'GET',
    url: '/api/posts',
    success: res => {
      let posts = res.data

      let appendHtml = ''

      posts.map( post => {
        appendHtml += `
          <tr data-id="${post.id}">
            <td></td>
            <td class="product-img"><img src="/storage/upload/posts/${post.image}" alt="post image">
            </td>
            <td class="product-name title-english">${post.title_english}</td>
            <td class="product-category content-english">${post.content_english}</td>
            <td class="product-name title-arabic">${post.title_arabic}</td>
            <td class="product-category content-arabic">${post.content_arabic}</td>
            <td class="product-action">
                <span class="action-edit"><i class="feather icon-edit"></i></span>
                <span class="action-delete"><i class="feather icon-trash"></i></span>
            </td>
        </tr>
        `
      })

      return $('table.posts-table tbody').html(appendHtml)
    },
    error: err => console.log(err)
  })
}

const addPost = () => {
  let title_english = $('input#post-title-english').val()
  let content_english = $('textarea#post-content-english').val()
  let title_arabic = $('input#post-title-arabic').val()
  let content_arabic = $('textarea#post-content-arabic').val()
  let image = document.querySelector('input#post-image').files[0]

  let data =  new FormData()
  data.append('title_english', title_english)
  data.append('content_english', content_english)
  data.append('title_arabic', title_arabic)
  data.append('content_arabic', content_arabic)
  data.append('image', image)
  data.append('_token', $('meta[name="csrf-token"]').attr('content'))
  $.ajax({
    type: 'POST',
    url: '/posts',
    enctype: "multipart/form-data",
    processData: false,
    contentType: false,
    data: data,
    success: res => {
      if(!res.success)
        alert('Seems you put invalid data')

      $('button#cancel-add-post').click()
      return getPosts()
    },
    error: err => console.log(err)
  })
}

const editPost = () => {
  let title_english = $('input#post-title-english').val()
  let content_english = $('textarea#post-content-english').val()
  let title_arabic = $('input#post-title-arabic').val()
  let content_arabic = $('textarea#post-content-arabic').val()
  let image = document.querySelector('input#post-image').files[0]

  let data =  new FormData()
  data.append('id', editId)
  data.append('title_english', title_english)
  data.append('content_english', content_english)
  data.append('title_arabic', title_arabic)
  data.append('content_arabic', content_arabic)
  data.append('_token', $('meta[name="csrf-token"]').attr('content'))
  if(!!image)
    data.append('image', image)

  $.ajax({
    type: 'POST',
    url: `/posts/edit`,
    enctype: "multipart/form-data",
    processData: false,
    contentType: false,
    data: data,
    success: res => {
      if(!res.success)
        alert('Seems you put invalid data')

      $('button#cancel-add-post').click()
      return getPosts()
    },
    error: err => console.log(err)
  })
}

const deletePost = id => {
  $.ajax({
    type: 'DELETE',
    url: `/posts/${id}`,
    data: {
      _token: $('meta[name="csrf-token"]').attr('content')
    },
    success: res => {
      return
    },
    error: err => console.log(err)
  })
}
